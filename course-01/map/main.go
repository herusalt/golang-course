package main

import "fmt"

type Orangtua struct {
	Ayah string
	Ibu  string
}

func main() {

	// Contoh pertama. array pertama adalah string, array kedua adalah string
	stringstring := make(map[string]string)

	stringstring["name"] = "TONO"
	stringstring["alamat"] = "PAMULANG"

	fmt.Println(stringstring)

	// Contoh kedua. array pertama adalah string, array kedua adalah int
	stringint := make(map[string]int)
	stringint["id"] = 1
	stringint["harga"] = 2000

	fmt.Println(stringint)

	//Contoh ketiga. array pertama adalah string, array kedua adalah any
	stringany := make(map[string]interface{})
	stringany["status"] = 200
	stringany["data"] = "HALO"

	fmt.Println(stringany)

	// Contoh keepmat, array pertama string, array kedua struct Orangtua

	stringstruct := make(map[string]Orangtua)
	stringstruct["Budi"] = Orangtua{
		"Ayah Suparno",
		"Ibu Siti",
	}

	fmt.Println(stringstruct)
}
