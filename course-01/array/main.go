package main

import "fmt"

func main() {

	// array string
	//var siswa [2]string

	// array string
	//var siswa = [2]string{}

	// siswa := [2]string{"Udin", "Toni","Budi"} ini salah karena komposisi array cuma 2

	siswa := [2]string{"Udin", "Toni"}

	// print semua siswa
	fmt.Println(siswa)

	// print siswa no 2
	fmt.Println(siswa[1])

	// contoh nilai array kelebihan
	berlebih := [4]int{1, 2, 3}

	fmt.Println(berlebih)

	berlebih2 := [2]string{"Halo"}

	fmt.Println(berlebih2)

}
