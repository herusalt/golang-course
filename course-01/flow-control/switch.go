package main

import "fmt"

func main() {

	nilai := 10

	switch nilai {
	case 1, 2, 3, 4, 5:
		fmt.Println("Nilai Jelek")
	case 6, 7, 8:
		fmt.Println("Nilai Lumayan")
	case 9, 10:
		fmt.Println("Sampurna")
	}

	nilai = 2

	switch {
	case (nilai >= 1) && (nilai <= 5):
		fmt.Println("Nilai Jelek V2")
	case (nilai >= 6) && (nilai <= 8):
		fmt.Println("Nilai Lumayan V2")
	case (nilai > 8):
		fmt.Println("Sampurna V2")
	}

}
