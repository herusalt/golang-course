package main

import "fmt"

func main() {

	// Cara penulisan variable
	//var i int = 24
	//shorthand #1
	//j := 76
	//shorthand #2
	k := int(52)

	kpointer := &k // Ambil pointer (blok memory) berupa hexa

	fmt.Println(kpointer) // tampilkan nilai berupa hexa

	fmt.Println(*kpointer) // tampilkan nilai berupa value a-z 0-9 menggunakan bintang (*)

	pointerkpointer := kpointer              // ambil pointer dari kpointer
	*pointerkpointer = *pointerkpointer + 22 // ubah pointer ke reference lalu ditambahkan 22

	// maka nilai k ada perubahan 52 + 22
	fmt.Println(k)
}
