package valueobject

import "golang.org/x/crypto/bcrypt"

type Password struct {
	Salt string
	Hash string
}

func (p *Password) GetSalt() string {
	return p.Salt
}

func (p *Password) GetHash() string {
	return p.Hash
}

func NewPassword(password string) *Password {
	passwordHash, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	return &Password{
		Hash: string(passwordHash),
	}
}
