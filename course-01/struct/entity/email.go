package entity

type Email struct {
	Local  string
	Domain string
}

func (e *Email) GetLocalEmail() string {
	return e.Local
}

func (e *Email) GetDomainEmail() string {
	return e.Domain
}

func NewEmail(local string, domain string) *Email {
	return &Email{
		local,
		domain,
	}
}
