package main

import (
	"example-struct/entity"
	"example-struct/usecase"
	"example-struct/valueobject"
	"fmt"
)

type SimpleStruct struct {
	Id   int
	Nama string
}

func main() {

	// Nilai 0
	fmt.Println(SimpleStruct{})

	// Kasih value ke struct
	fmt.Println(SimpleStruct{1, "Nama"})

	dataBaru := usecase.NewUser(
		"Heru Rahmat",
		entity.NewEmail("heru", "@salt.co.id"),
		valueobject.NewPassword("12345"),
	)

	email := dataBaru.Email.GetLocalEmail() + dataBaru.Email.Domain
	fmt.Println(email, dataBaru.Name, dataBaru.Password.GetHash())
}
