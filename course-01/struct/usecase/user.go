package usecase

import (
	"example-struct/entity"
	"example-struct/valueobject"
)

type Users struct {
	Email    *entity.Email
	Name     string
	Password *valueobject.Password
}

func NewUser(name string, email *entity.Email, password *valueobject.Password) *Users {
	return &Users{
		email,
		name,
		password,
	}
}
